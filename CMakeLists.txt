cmake_minimum_required(VERSION 3.1 FATAL_ERROR)

message("______________________________________________________")

project(synctables   VERSION 1.1 LANGUAGES CXX)     #todo: get vers from git
message("Project: ${PROJECT_NAME} ${PROJECT_VERSION}")

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS_DEBUG          "-O0 -g")
set(CMAKE_CXX_FLAGS_RELEASE        "-O3 -DNDEBUG")

##set(ignoreMe "${CMAKE_C_COMPILER}${QT_QMAKE_EXECUTABLE}")

#функции для cmake
include(cmake/functions.cmake)

#каталоги результатов
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/bin)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR}/lib)

add_custom_target(makedirs ALL
  #здесь будут библиотеки
  COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_SOURCE_DIR}/lib
  #здесь будут их хедеры
  COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_SOURCE_DIR}/lib_include
)


#можно включить(ON)/выключить(OFF) пересборку готовых библиотек
option(BUILD_SELF_LIBS "Build the self library(s)" ON)
#set(BUILD_SELF_LIBS ON)

#исходники
add_subdirectory(${CMAKE_SOURCE_DIR}/src)



