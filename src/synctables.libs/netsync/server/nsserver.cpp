#include "nsserver.h"

#include <QDataStream>
#include <QHostAddress>
#include <QtNetwork>
#include <QTimer>

NSServer::NSServer(quint16 listeningPort)
    : QObject()
    , listeningPort(listeningPort)
{
}

NSServer::~NSServer()
{
    if (netSocket)
    {
        netSocket->abort();
        netSocket->deleteLater();
    }

    netServer.close();
}

bool NSServer::initNetServer()
{
    if ( !netServer.listen(QHostAddress::Any, listeningPort) )
    {
        netErrorString = netServer.errorString();
        return false;
    }

    netServer.setMaxPendingConnections(1); //we want only the one client
    connect( &netServer, &QTcpServer::newConnection, this, &NSServer::onNewClientConnection);

    return true;
}


bool NSServer::isConnected() const
{
    return netSocket && netSocket->state() == QAbstractSocket::ConnectedState;
}

const QString &NSServer::getNetErrorString() const
{
    return netErrorString;
}

void NSServer::doDisconnect()
{
    if ( isConnected() )
        netSocket->disconnectFromHost();
}


bool NSServer::isReadyForConnection() const
{
    return getNetErrorString().isEmpty() && !isConnected();
}


void NSServer::onNewClientConnection()
{
    if ( !netServer.hasPendingConnections() )
        return;

    if ( isConnected() ) //accept only one connection
    {//reject new connection
        QTcpSocket *clientConnection = netServer.nextPendingConnection();
        if (clientConnection)
        {
            clientConnection->disconnectFromHost();
            clientConnection->deleteLater();
        }
    }
    else
    {//accept
        netSocket = netServer.nextPendingConnection();
        if (!netSocket)
            return;

        //netSocket->setSocketOption(QAbstractSocket::KeepAliveOption, 1);
        connect(netSocket, &QTcpSocket::disconnected, this, &NSServer::onClientDisconnected);
        connect(netSocket, &QTcpSocket::readyRead, this, &NSServer::onIncomingData);

        emit clientConnected();
    }
}


void NSServer::onClientDisconnected()
{
    if (netSocket)
    {
        disconnect(netSocket, &QTcpSocket::disconnected, this, &NSServer::onClientDisconnected);
        disconnect(netSocket, &QTcpSocket::readyRead,    this, &NSServer::onIncomingData);
        netSocket->deleteLater();

        netSocket = nullptr;
    }

    emit clientDisconnected();
}


qint64 NSServer::sendData(const QVariant &blob) const
{
    if ( !blob.isValid() || blob.isNull() || !isConnected() )
        return -1;

    return netSocket->write( blob.toByteArray() );
}


void NSServer::onIncomingData()
{
    QTcpSocket *clientSocket = static_cast<QTcpSocket*>(sender());  //netSocket;

    const auto &blob = clientSocket->readAll();
    emit newDataReady(blob);
}


