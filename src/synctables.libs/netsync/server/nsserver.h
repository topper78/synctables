#pragma once

#include <QTcpServer>
#include <QTcpSocket>
#include <QString>
#include <QVariant>

class NSServer final : public QObject
{
    Q_OBJECT

public:
    NSServer(quint16 listeningPort);
    ~NSServer();

    bool isConnected() const;
    bool isReadyForConnection() const;

    const QString& getNetErrorString() const;
    bool initNetServer();

public slots:
    void doDisconnect();
    qint64 sendData(const QVariant &blob) const;

signals:
    void clientDisconnected() const;
    void clientConnected() const;
    void newDataReady(const QVariant &blob);


private slots:
    void onNewClientConnection();
    void onClientDisconnected();
    void onIncomingData();

//members
private:
    quint16 listeningPort = 0;
    QString netErrorString;

    QTcpServer netServer;
    QTcpSocket *netSocket = nullptr; //one for one Client
};

