#pragma once

#include "client/nsclient.h"
#include "server/nsserver.h"

#include <QObject>
#include <QString>
#include <QVariant>

class NetSync : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool appsConnected
               READ isAppsConnected
               NOTIFY appsConnectionStatusChanged
               )

    Q_PROPERTY(bool isServer
               READ isServerState
               NOTIFY serverStatusChanged
               )


public:
    explicit NetSync(QObject *parent = nullptr);
    virtual ~NetSync();

public slots:
    bool isAppsConnected() const;
    bool isServerState() const;
    Q_INVOKABLE qint64 sendData(const QVariant &blob) const;
    Q_INVOKABLE void initConnect();

signals:
    void netSyncLogMessage(const QString &message) const;
    void appsConnectionStatusChanged(bool newStatus) const;
    void serverStatusChanged(bool newStatus) const;
    void syncDataReady(const QString &incomingJSON) const;

private:
    bool loadSettings();
    void cleanNetStuff();

private slots:
    bool becameClient();
    void onBecomingClientFail();
    bool becameServer();
    void onConnected() const;
    void onDisconnected();
    void onIncomingData(const QVariant &blob) const;

    void emitLogMessage(const QString &message) const;


//members
private:
    quint16 connectionPort = 50001;
    QString hostAddress = "127.0.0.1";
    int connectionTimeoutMSec = 2000;
    bool clientBecameServerOnConnectionFail = true;

    NSClient *client = nullptr;
    NSServer *server = nullptr;

    bool serverIsLocal = true;
};
