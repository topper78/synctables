#include "NetSync.h"

#include <QDebug>
#include <QSettings>
#include <QTimer>
#include <QHostInfo>
#include <QNetworkInterface>
#include <QDir>
#include <QThread>

NetSync::NetSync(QObject *parent)
    : QObject(parent)
{
    if ( loadSettings() )
    {
        serverIsLocal = QHostInfo::localHostName() == hostAddress  ||                             //if used name in settings
                        QNetworkInterface::allAddresses().contains( QHostAddress(hostAddress) );  //if used IP in settings
    }
}

NetSync::~NetSync()
{
    cleanNetStuff();
}


bool NetSync::isAppsConnected() const
{
    return ( client && client->isConnected() ) || ( server && server->isConnected() );
}

bool NetSync::isServerState() const
{
    return (server);
}



bool NetSync::loadSettings()
{
#if defined( Q_OS_LINUX ) || defined( Q_OS_UNIX )
    QSettings settings; //$HOME/<orgname>/<appname>.conf
#else
    QSettings settings(QDir::currentPath()+"/synctables.conf", QSettings::IniFormat);
#endif

    connectionPort = static_cast<quint16>( settings.value("connectionPort", connectionPort).toUInt() );
    hostAddress = settings.value( "hostAddress", QHostInfo::localHostName() ).toString();
    connectionTimeoutMSec = settings.value("connectionTimeoutMSec", connectionTimeoutMSec).toInt(); //default 2 sec
    clientBecameServerOnConnectionFail = settings.value("clientBecameServerOnConnectionFail",
                                                        clientBecameServerOnConnectionFail).toBool();

    return true;
}


void NetSync::initConnect()
{
    if ( serverIsLocal )
    {
        if ( !becameServer() )
            becameClient();
    }
    else //want remote server
    {
        if ( !becameClient() ) //ex mem err
            becameServer(); //todo: we want to local server?
    }
}

void NetSync::cleanNetStuff()
{
    if (server)
    {
        //+- disconnect signals
        server->doDisconnect();

        server->deleteLater();
        server = nullptr;
    }
    if (client)
    {
        disconnect(client, &NSClient::connectionTimeout, this, &NetSync::becameServer);
        disconnect(client, &NSClient::connected, this, &NetSync::onConnected);
        disconnect(client, &NSClient::disconnected, this, &NetSync::onDisconnected);
        disconnect(client, &NSClient::newDataReady, this, &NetSync::onIncomingData);
        client->doDisconnect();

        client->deleteLater();
        client = nullptr;
    }
}

bool NetSync::becameClient()
{
    cleanNetStuff();
    client = new NSClient( hostAddress, connectionPort, connectionTimeoutMSec );
    if (!client)
    {
        emitLogMessage( tr("ERROR: Can`t became the client!!! (no mem?) ") );
        return false;
    }
    else
    {
        emit serverStatusChanged(false);

        emitLogMessage( tr("Trying connect to the server (Addr=%1, port=%2; timeout=%3 msec)...")
                          .arg(hostAddress).arg(connectionPort).arg(connectionTimeoutMSec) );

        connect(client, &NSClient::connectionTimeout, this, &NetSync::onBecomingClientFail);
        connect(client, &NSClient::connected,         this, &NetSync::onConnected);
        connect(client, &NSClient::disconnected,      this, &NetSync::onDisconnected);
        connect(client, &NSClient::newDataReady,      this, &NetSync::onIncomingData);

        client->connectToHost();

        return true;
    }
}

void NetSync::onBecomingClientFail()
{
    emitLogMessage( tr("Connecting to the server FAILED!") );

    ///serverStatusChanged ???

        //TODO: reconnect?
        QThread::sleep(1);   //if started more than 2 instance
        if (clientBecameServerOnConnectionFail)
            becameServer();
        else
            becameClient(); //initConnect();  //TODO: try and try to connecting to a server  OR  just becameServer() (became the local server)?
}

bool NetSync::becameServer()
{
    cleanNetStuff();
    server = new NSServer(connectionPort);
    if (!server || !server->isReadyForConnection() ||
        !server->initNetServer()                      //if one instance (server) is started - listen() will fail, then we start as Client
       )
    {
        emitLogMessage( tr("Can`t became the server (port=%1)").arg(connectionPort) );
        if (server)
            emitLogMessage( tr("Reason: ")+server->getNetErrorString() );
        return false;
    }

    //all ok
    connect(server, &NSServer::clientDisconnected,
            [this]() {
               emit appsConnectionStatusChanged(false);
               emitLogMessage("The client was disconnected!");
            }
    );
    connect(server, &NSServer::clientConnected, this, &NetSync::onConnected);
    connect(server, &NSServer::newDataReady, this, &NetSync::onIncomingData);

    emit serverStatusChanged(true);

    emitLogMessage( tr("Became the server (port=%1). Waiting for a client...").arg(connectionPort) );

    return true;
}

void NetSync::onConnected() const
{
    if ( isServerState() )
        emitLogMessage( tr("A client is connected.") );
    else //isClient
        emitLogMessage( tr("Succefully connected to the server.") );

    emit appsConnectionStatusChanged(true);
}

void NetSync::onDisconnected()
{
    emitLogMessage( tr("DISCONNECTED from the server!") );
    emit appsConnectionStatusChanged(false);
         //TODO: reconnect?
         QThread::sleep(1);   //if started more than 2 instance
         if (clientBecameServerOnConnectionFail)
             becameServer();
         else
             becameClient(); //initConnect();  //TODO: try and try to connecting to a server  OR  just becameServer() (became the local server)?
}


void NetSync::emitLogMessage(const QString &message) const
{
    if ( message.isEmpty() )
        return;

    //TODO: here may add some info into src message
    emit netSyncLogMessage( message );
}


void NetSync::onIncomingData(const QVariant &blob) const
{
    const auto &json = blob.toString();
    emitLogMessage( tr("Received %1 bytes of Data").arg( json.size() ) );

    emit syncDataReady( blob.toString() );
}

qint64 NetSync::sendData(const QVariant &blob) const  //TODO: There is good for test task), but need refactoring for highloaded system//
{
    if ( !isAppsConnected() )
        return -1;

    qint64 bytesSent = 0;
    if ( isServerState() )
    {
        bytesSent = server->sendData(blob);
    }
    else //isClient
    {
        bytesSent = client->sendData(blob);
    }

    if (bytesSent < 0)
        return bytesSent;

    emitLogMessage( tr("Sent %1 bytes of Data").arg(bytesSent) );

    return bytesSent;
}


