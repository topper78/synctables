#pragma once

#include <QObject>
#include <QString>
#include <QTimer>
#include <QTcpSocket>
#include <QVariant>

class NSClient final : public QObject
{
    Q_OBJECT

public:
    NSClient(const QString &hostAddress, quint16 hostPort, int connectionTimeoutMSec = 1000);
    ~NSClient();

    bool isConnected() const;

public slots:
    void doDisconnect();
    void connectToHost();
    qint64 sendData(const QVariant &blob) const;

signals:
    void connectionTimeout() const;
    void connected() const;
    void disconnected() const;
    void newDataReady(const QVariant &blob);


private slots:
    void onConnectionTimeout();
    void onDisconnected();
    void onIncomingData();

//members
private:
    QString hostAddress;
    quint16 hostPort = 0;
    int connectionTimeoutMSec = 0;
    QTimer connectionTimeoutTimer;
    mutable QTcpSocket netSocket;
};

