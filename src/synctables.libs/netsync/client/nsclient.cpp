#include "nsclient.h"

#include <QCoreApplication>
#include <QtNetwork>

NSClient::NSClient(const QString &hostAddress, quint16 hostPort, int connectionTimeoutMSec)
    : QObject()
    , hostAddress(hostAddress)
    , hostPort(hostPort)
    , connectionTimeoutMSec(connectionTimeoutMSec)
{
    connectionTimeoutTimer.setSingleShot( true );
    connectionTimeoutTimer.callOnTimeout( this, &NSClient::onConnectionTimeout );

    connect(&netSocket, &QTcpSocket::connected, this, &NSClient::connected);
    connect(&netSocket, &QTcpSocket::disconnected, this, &NSClient::onDisconnected);
    connect(&netSocket, &QTcpSocket::readyRead, this, &NSClient::onIncomingData);
    connect(&netSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(onConnectionTimeout()), Qt::DirectConnection);   //on error we want disconnect like timeout
}

NSClient::~NSClient()
{
    doDisconnect();
    netSocket.abort();
}


bool NSClient::isConnected() const
{
    return netSocket.state() == QAbstractSocket::ConnectedState;
}

void NSClient::doDisconnect()
{
    if ( isConnected() )
        netSocket.disconnectFromHost();
}

void NSClient::connectToHost()
{
    if ( connectionTimeoutMSec > 0 )
        connectionTimeoutTimer.start(connectionTimeoutMSec);

    netSocket.abort();
    netSocket.connectToHost(hostAddress, hostPort);
}

void NSClient::onConnectionTimeout()
{
    connectionTimeoutTimer.stop();

    if ( netSocket.state() != QAbstractSocket::ConnectedState )
    {
        disconnect(&netSocket, &QTcpSocket::connected, this, &NSClient::connected);
        disconnect(&netSocket, &QTcpSocket::disconnected, this, &NSClient::onDisconnected);
        disconnect(&netSocket, &QTcpSocket::readyRead, this, &NSClient::onIncomingData);

        netSocket.flush();
        netSocket.disconnectFromHost();

        //if (netSocket.state() != QAbstractSocket::UnconnectedState)
        //     if (!netSocket.waitForDisconnected())   //TODO: so long..., and not work with pointed timeout  //may be ping serv before...
        //         netSocket.abort();

        emit connectionTimeout();
    }
}

void NSClient::onDisconnected()
{
    connectionTimeoutTimer.stop();
    netSocket.abort();

    emit disconnected();
}


qint64 NSClient::sendData(const QVariant &blob) const
{
    if ( !blob.isValid() || blob.isNull() || !isConnected() )
        return -1;

    return netSocket.write( blob.toByteArray() );
}

void NSClient::onIncomingData()
{
    const auto &blob = netSocket.readAll();
    emit newDataReady(blob);
}


