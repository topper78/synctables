import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4 as QQC1

ColumnLayout {
    id: connectionInfoLayout
    spacing: 0
    Layout.fillWidth : true

    property alias taNetLog: taNetLog

    Label {
        text: qsTr("Connection info:")
    }

    Frame {
        Layout.fillWidth : true
        Layout.maximumHeight: 50
        Layout.minimumHeight: 50
        bottomPadding: 0
        topPadding: 0
        padding: 2
        
        ScrollView {
            anchors.fill: parent
            clip: true
            ScrollBar.vertical.policy: ScrollBar.AlwaysOn
            bottomPadding: 0
            topPadding: 0
            wheelEnabled: true
            
            TextArea {
                id: taNetLog
                readOnly: true
                background: null
                wrapMode: Text.WordWrap
                bottomPadding: 0
                topPadding: 0
                selectByMouse : true
                selectionColor: "blue"
                font.pixelSize: 12
            }
        }//ScrollView
    }//frame
}
