import QtQuick 2.12
import QtQuick.Window 2.12
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.12
import QtQuick.Controls.Styles 1.4

import synctables.PTModel 1.0
//import Qt.labs.qmlmodels 1.0

Rectangle {
    id : tvBorder

    property alias dataSource: ptDispatcherJSON.source
    property alias json: ptDispatcherJSON.json
    property alias tableView: tableView
    property bool fNeedToSaveJSONfile: false

    border { color: "gray"; width:1; }
    color: "transparent"
    Layout.fillHeight: true
    Layout.fillWidth: true


    TableView {
        id : tableView
        selectionMode: SelectionMode.SingleSelection //SelectionMode.NoSelection
        anchors.fill: parent
        focus: true
        clip: true
        visible: true

        signal personTableViewModyfied();  //(changes from view to model, not backward)

        TableViewColumn {
             id: colID
             role: "ID"
             title: "<b>"+qsTr(role)+"</b>"
             horizontalAlignment: Text.AlignHCenter
        }

        TableViewColumn {
             id: colName
             role: "Name"
             title: "<b>"+qsTr(role)+"</b>"
             horizontalAlignment: Text.AlignHCenter
        }

        TableViewColumn {
             id: colPhone
             role: "Telefon"
             title: "<b>"+qsTr(role)+"</b>"
             horizontalAlignment: Text.AlignHCenter
        }

        TableViewColumn {
             id: colCountry
             role: "Country"
             title: "<b>"+qsTr(role)+"</b>"
             horizontalAlignment: Text.AlignHCenter
             width: tableView.viewport.width - colID.width - colName.width - colPhone.width  //stretch last col
        }

        model: PTModel { id: ptModel  }
        Connections {
            target: ptModel
            onDataChanged: {
                if ( fNeedToSaveJSONfile )
                    ptDispatcherJSON.saveJSONFromModel();
                else
                    ptDispatcherJSON.pureSetJsonProperty( JSON.stringify( ptModel.dataToJSON(), null, 4 ) );
            }
        }

        itemDelegate: PTDelegate {
            id: ptDelegate

            Connections {
                onRequestAppendEmptyDataRow: ptModel.appendEmptyDataRow()
                onRequestRemoveDataRow: {
                    ptModel.removeRow(row);
                    tableView.personTableViewModyfied();  //emit signal
                }
                onRequestSetData: {
                    ptModel.setData( ptModel.index(row, col), text, col );
                    tableView.personTableViewModyfied();  //emit signal
                }
            }
            Component.onCompleted: { ptModel.emptyDataRowReady.connect(ptDelegate.onNewRow); }
        }//delegate


        PTDispatcherJSON {
            id: ptDispatcherJSON
            source: dataSource
            dataModel : ptModel
        }

    }//TableView

    Menu {
          id: ptContextMenu
          MenuItem {
              text: qsTr("Add new data row")
              onTriggered: {
                  ptModel.appendEmptyDataRow();
              }
          }
    }//Menu

    MouseArea {
         id: tvMouseArea
         z:-1
         acceptedButtons:  Qt.RightButton //Qt.LeftButton | Qt.RightButton //Qt.AllButtons
         anchors.fill: parent

         onClicked: {
             forceActiveFocus();
             if ( mouse.button === Qt.RightButton )
             {
                ptContextMenu.popup();
             }
         }
    }//tvMouseArea

}//rectangle


