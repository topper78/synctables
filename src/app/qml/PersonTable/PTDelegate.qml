import QtQuick 2.12
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.12


Rectangle {
    id : rectItemDelegate
    color: colorByRow() //  "transparent"
    border.width: 1
    anchors.margins: 0

    function colorByRow() {
        if (styleData.row%2 == 0)
            return "whitesmoke";
        else
            return "white";
    }

    signal requestAppendEmptyDataRow();
    signal requestRemoveDataRow(var row);
    signal requestSetData(var row, var col, var text);

    Text {
        id: delegateText
        anchors { verticalCenter: parent.verticalCenter; left: parent.left; margins : 1 }
        text: styleData.value === undefined? "" : styleData.value     //+" |"+styleData.row+'|'+styleData.column
    }

    Menu {
          id: ptDelegateContextMenu
          MenuItem {
              text: qsTr("Add new data row")
              onTriggered: {
                  requestAppendEmptyDataRow();
              }
          }
          MenuItem {
              text: qsTr("Remove selected data row")
              onTriggered: {
                  requestRemoveDataRow(styleData.row);
              }
          }
    }//Menu

    MouseArea {
        id: cellMouseArea
        acceptedButtons:  Qt.LeftButton | Qt.RightButton
        anchors.fill: parent
        propagateComposedEvents: true

        onClicked: {
            forceActiveFocus();
            if ( mouse.button === Qt.RightButton )
            {
               ptDelegateContextMenu.popup()
            }
            else
            {
                mouse.accepted = false;
            }
        }

        onActiveFocusChanged: {
            if (activeFocus)
            {
              //need key handle//
                rectItemDelegate.color = "skyblue"// "#55eef0"
            }
            else
            {
              //need key handle//
                rectItemDelegate.color = colorByRow()
            }
        }

        onDoubleClicked: {
                forceActiveFocus()
            ///if (styleData.column === 0)   //zero based
                loader.visible = true
                loader.item.forceActiveFocus()
        }

    }//MouseArea


    function onNewRow(targetRow) {
        if (styleData.row !== targetRow  || styleData.column !== 0)
            return;

        forceActiveFocus()
        loader.visible = true
        loader.item.forceActiveFocus()
    }


    //EDIT PURPOSE
    Loader {
        id: loader
        anchors.fill: parent
        height: parent.height
        width: parent.width
        visible: false
        sourceComponent: visible ? lineEdit : undefined
    }
    Component {
        id: lineEdit
        TextInput {  //TextField is bad (text positions)
            anchors.fill: parent
            anchors.margins: 1
            clip: true
            focus: true
            selectByMouse: true
            text: styleData.value === undefined? "" : styleData.value
            //inputMask: styleData.column === 0? "99999" : ""

            Rectangle { //hide back text
                visible: true
                z:-1
                anchors.fill: parent
            }

            Keys.onEscapePressed: reject();

            onEditingFinished: {  //onAccepted more stong (want ENTER)
                //ACCEPT
                requestSetData(styleData.row, styleData.column, text);
                loader.visible = false;
            }

            onActiveFocusChanged: {
                if (!activeFocus)
                    reject()
                //else
                //    selectAll();
            }

            function reject() {
                loader.visible = false
            }

        }//TextInput
    }//Component

}//itemDelegate (Rectangle)
