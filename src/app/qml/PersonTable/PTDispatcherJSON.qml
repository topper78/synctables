import QtQuick 2.12
import synctables.PTModel 1.0

import MessageBox 1.0

Item {
    id: rootObj
    property string source: ""  //[waited]
    property string json: ""
    //property ListModel model: ListModel { id: dataModel }
    property var dataModel: parent.model //[required]
    property MessageBox messageBox: MessageBox {}
    property bool enableUpdateModel: true

    onSourceChanged: {
        console.log('loading JSON data from ', source);
        var xhr = new XMLHttpRequest;
        xhr.open("GET", source);
        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE)
            {
                pureSetJsonProperty( xhr.responseText );
                updateModelFromJSON(true); //true = here only load, no need to save
            }
        }
        xhr.send();
    }

    Connections {
        target: rootObj
        onJsonChanged: { updateModelFromJSON(); }
        enabled: enableUpdateModel            //like blockSignals(bool) in Qt
    }

    function updateModelFromJSON(fDontEmitDataChanged) {
        dataModel.clear();

        if ( json === "" )
            return;

        try {
            var jStr = JSON.parse(json);
        }
        catch(e) {
            messageBox.showMessage( qsTr("JSON LOADING..."), qsTr("FAILED!\n")+e );
            return;
        }

        dataModel.appendFromJSON( jStr,
                                 (typeof fDontEmitDataChanged !== "undefined"?
                                                 fDontEmitDataChanged : false) );
/*
        var objectArray = JSON.parse(json);
        var personArray = objectArray["Person"]  //objectArray.Person;
        for (var i = 0; i < personArray.length; i++)
        {
            dataModel.append( personArray[i] );
        }
*/
    }

    function pureSetJsonProperty(newJson) {
        enableUpdateModel = false;
        json = newJson;
        enableUpdateModel = true;
    }

    function saveJSONFromModel() {
        if (source === "")
            return;

        console.log('saving JSON data to ', source);
        var xhr = new XMLHttpRequest();
        var newJson = JSON.stringify( dataModel.dataToJSON(), null, 4 );

        pureSetJsonProperty(newJson);

        xhr.open("PUT", source);
        xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
        xhr.onerror = function() {  //TODO: ITS NOT WORKED!!!
            messageBox.showMessage( qsTr("JSON SAVING..."), qsTr("FAILED!") );
        };
        xhr.onloadend = function() {
            console.log("status of json saving: "+xhr.status+': '+xhr.responseText+': '+xhr.statusText);
        }
/*      xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE) {
                console.log("status of json saving: "+xhr.status+': '+xhr.responseText+': '+xhr.statusText);
            }
        }
*/
        xhr.send( newJson );
    }//saveJSONFromModel()

}//root
