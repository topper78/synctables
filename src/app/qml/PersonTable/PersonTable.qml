import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
//import QtQuick.Controls 1.4 as QQC1

ColumnLayout {
    id: rootObj

    property alias dataSource: personTableView.dataSource
    property alias json: personTableView.json
    property alias fNeedToSaveJSONfile: personTableView.fNeedToSaveJSONfile

    spacing: 0
    Layout.fillWidth : true
    Layout.fillHeight: true

    signal personTableModyfied(); //reemit


    Label {text: qsTr("Person data:") }

    PersonTableView {
        id : personTableView
        focus: true
        dataSource: rootObj.dataSource

        Connections {
            target: personTableView.tableView
            onPersonTableViewModyfied: personTableModyfied();
        }
    }

}//rootObj
