import QtQuick 2.2
import QtQuick.Dialogs 1.3
//import Qt.labs.platform 1.1

MessageDialog {
    id: messageBox
    title: ""
    text: ""
    modality: Qt.ApplicationModal
//    informativeText: "inf text"
//    detailedText: "det text in bottom box"
//    icon: StandardIcon.Question
    visible: false
//    standardButtons: StandardButton.Yes | StandardButton.YesToAll | StandardButton.No | StandardButton.Abort

    function showMessage(title, text, buttons)
    {
        messageBox.title = title;
        messageBox.text = text;
        if (buttons)
            messageBox.standardButtons = buttons;
        messageBox.open();
    }
}
