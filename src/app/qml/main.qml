import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import QtQuick.Controls 1.4 as QQC1

import Qt.labs.settings 1.0

//QML modules
import PersonTable 1.0
import ConnectionInfo 1.0

//C++ libs
import synctables.NetSync 1.0


ApplicationWindow {
    id : mainWindow
    objectName: "MainWindow"
    visible: true
    minimumWidth: 640
    minimumHeight: 480
    width: minimumWidth
    height: minimumHeight
    title: qsTr("SyncTables")
    property string jsonDataFile :  ( typeof jsonDataFileFromSetting !== "undefined" ) ?
                                     jsonDataFileFromSetting :     //get from main.cpp (QML Settings is very bad)
                                     "persData.json";              //DEFAULT PATH/NAME


/*
    QQC1.SplitView {
        anchors.fill: parent
        orientation: Qt.Vertical
        anchors.margins: 5
        GroupBox {
            id: gbConnectionInfo
        }
        GroupBox {
            id: table
        }
    }
*/

    ColumnLayout {
        anchors.fill: parent
        anchors.margins: 5

        //gui
        ConnectionInfo {
            id: connectionInfo
            visible: true
        }

        PersonTable {
            id: personTable
            visible: true
            focus: true

            onPersonTableModyfied: {
                if ( !netSync.appsConnected )
                    return;

                netSync.sendData( personTable.json );
            }
        }


        //net backend
        NetSync {
            id: netSync
            onNetSyncLogMessage: connectionInfo.taNetLog.append( message );

            onAppsConnectionStatusChanged: {
                if ( !appsConnected )
                    return;

                if ( isServer )
                {
                    sendData( personTable.json );
                }
                else //isClient
                {
                    console.log("waiting incoming data...");
                }
            }

            onSyncDataReady: {
                personTable.json = incomingJSON;
            }

            onServerStatusChanged: {
                if ( isServer )
                {
                    //loading data...
                    personTable.dataSource = 'file:' + jsonDataFile;
                }
                else //client
                {
                    connectionInfo.taNetLog.append( qsTr("I`m a Client, waiting for data from the Server...") );
                }

                personTable.fNeedToSaveJSONfile = isServer;
                personTable.visible = true;
            }

        }//NetSync

    }//column layout

    Component.onCompleted: netSync.initConnect();
}//appWin




















