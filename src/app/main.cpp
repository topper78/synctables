#include <QGuiApplication>
#include <QSettings>
#include <QDir>
//#include <QStandartPaths>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QtQml>

#include "netsync/NetSync.h"
#include "PTModel.h"

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    app.setOrganizationName("Home");
    app.setApplicationName("synctables");

    qmlRegisterType<NetSync>("synctables.NetSync", 1, 0, "NetSync");
    qmlRegisterType<PTModel>("synctables.PTModel", 1, 0, "PTModel");


#if defined( Q_OS_LINUX ) || defined( Q_OS_UNIX )
    QSettings settings; //$HOME/<orgname>/<appname>.conf
#else
    QSettings settings(QDir::currentPath()+"/synctables.conf", QSettings::IniFormat);
#endif

    const QString jsonDataFileFromSetting = settings.value("jsonDataFile").toString();

    QQmlApplicationEngine engine;
    engine.addImportPath(":/qml");
    if ( !jsonDataFileFromSetting.isEmpty() )
        engine.rootContext()->setContextProperty("jsonDataFileFromSetting", jsonDataFileFromSetting);

    const QUrl url(QStringLiteral("qrc:/qml/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl)
                           {
                               if (!obj && url == objUrl)
                                   QCoreApplication::exit(-1);
                           }, Qt::QueuedConnection);
    engine.load(url);

    return app.exec();
}
