#pragma once

#include <QObject>
#include <QStandardItemModel>
#include <QModelIndex>
#include <QHash>
#include <QByteArray>
#include <QVariant>
#include <QJsonObject>
#include <QDebug>


class PTModel : public QStandardItemModel
{
    Q_OBJECT

public:
    enum PTColumns
    {
        colID = 0,
        colName,
        colTelefon,
        colCountry,

        colAmounts
    };

    explicit PTModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;

    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
    bool setData(const QModelIndex &index, const QVariant &value, int role) override;

    QHash<int, QByteArray> roleNames() const override;

    Q_INVOKABLE void clear();
    Q_INVOKABLE void appendFromJSON(const QJsonObject &rootJsonObj, bool fDontEmitDataChanged = false);
    Q_INVOKABLE void appendEmptyDataRow();
    Q_INVOKABLE void removeRow(int row);
    Q_INVOKABLE QJsonObject dataToJSON() const;

///    Q_INVOKABLE void setHeader(const QVector<QString> &header);

signals:
    void emptyDataRowReady(int rowNum);

private:
    QHash<int, QByteArray> tableRoles;
};
