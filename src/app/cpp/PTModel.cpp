#include "PTModel.h"

#include <QJsonArray>
#include <QJsonValue>
#include <QJsonDocument>

PTModel::PTModel(QObject *parent)
    : QStandardItemModel(parent)
{
    //TODO: read JSON data before, then init roles   =   dynamic columns
    tableRoles[colID] = "ID";
    tableRoles[colName] = "Name";
    tableRoles[colTelefon] = "Telefon";
    tableRoles[colCountry] = "Country";

//    setHeaderData(colID, Qt::Horizontal, tableRoles[colID], colID);
//    setHeaderData(colName, Qt::Horizontal, tableRoles[colName], colName);
//    setHeaderData(colTelefon, Qt::Horizontal, tableRoles[colTelefon], colTelefon);
//    setHeaderData(colCountry, Qt::Horizontal, tableRoles[colCountry], colCountry);

}


int PTModel::rowCount(const QModelIndex &parent) const
{
    return QStandardItemModel::rowCount(parent);
}

int PTModel::columnCount(const QModelIndex &parent) const
{
    return QStandardItemModel::columnCount(parent);
}


QVariant PTModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    const QModelIndex &modelIndex = QStandardItemModel::index(index.row(), role);
    return QStandardItemModel::data( modelIndex, Qt::DisplayRole);
}

bool PTModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if ( index.isValid() )
    {
         if ( data(index, role) != value.toString() )
         {
             const QModelIndex &modelIndex = QStandardItemModel::index(index.row(), role);
             blockSignals(true);
             QStandardItemModel::setData( modelIndex, value, Qt::DisplayRole );
             blockSignals(false);
             emit dataChanged(modelIndex, modelIndex, {role} );
             return true;
         }
     }
     return false;
}

QHash<int, QByteArray> PTModel::roleNames() const
{
    return tableRoles;
}

void PTModel::clear()
{
    QStandardItemModel::clear();
}

void PTModel::appendFromJSON(const QJsonObject &rootJsonObj, bool fDontEmitDataChanged)
{
    const QJsonArray &personsData = rootJsonObj["Person"].toArray();
    const int insPosition = rowCount();
    const int insRowsAmount = personsData.count();

    if (insRowsAmount > 0 )
    {
        int curRowIndex = insPosition;
        beginInsertRows({}, insPosition, insPosition + insRowsAmount - 1);
        for( const QJsonValue iPersonDict : personsData )
        {
            QList<QStandardItem *> dataItems;
            dataItems.reserve( PTColumns::colAmounts );              //TODO: dynamic columns amount  is  iPersonDict.toObject().keys().count()
            for (int iCol = 0; iCol < PTColumns::colAmounts; ++iCol) //TODO: dynamic columns amount  is  iPersonDict.toObject().keys().count()
            {
                auto pItem = new QStandardItem();
                pItem->setData( iPersonDict.toObject().value( tableRoles[iCol] ), Qt::DisplayRole );
                dataItems.push_back( pItem );
            }

            insertRow( curRowIndex++, dataItems );
        }
        endInsertRows();
    }

    layoutChanged();

    if (!fDontEmitDataChanged)
        emit dataChanged( index(insPosition, 0),    index(insPosition + insRowsAmount - 1, 0) );
}

void PTModel::appendEmptyDataRow()
{
    //TODO: chk only one empty row (if need)
    QList<QStandardItem *> dataItems;
    dataItems.reserve( tableRoles.count() );      //PTColumns::colAmounts
    for (int i = 0; i  < tableRoles.count(); ++i) //PTColumns::colAmounts
    {
        auto pItem = new QStandardItem();
        dataItems.push_back( pItem );
    }
    appendRow(dataItems);
    layoutChanged();
    emit emptyDataRowReady( rowCount()-1 );
}

void PTModel::removeRow(int row)
{
    QStandardItemModel::removeRow(row);
    emit dataChanged(index(0,0), index(rowCount()-1, columnCount()-1));
}

QJsonObject PTModel::dataToJSON() const
{
    QJsonObject rootObj;

    const int dataRowsAmount = rowCount();
    if ( dataRowsAmount<=0 )
        return rootObj;

    QJsonArray personsData;
    for(int iRow = 0; iRow < rowCount(); ++iRow)
    {
        QJsonObject iPersonData;
        for (int jCol = 0; jCol < tableRoles.count(); ++jCol)
        {
            iPersonData[ tableRoles[jCol] ] = QStandardItemModel::data( index(iRow,jCol) ).toJsonValue();
        }
        personsData << iPersonData;
    }

    rootObj["Person"] = personsData;

//or ret: QByteArray   QJsonDocument(rootObj).toJson( QJsonDocument::Indented );  and no stringify!

    return rootObj;
}


//void PTModel::setHeader(const QVector<QString> &header)
//{
//    tableData[0] = header;
//}
