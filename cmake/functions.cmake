#добавляем цель библиотеку
function(custom_add_library_from_dir _TARGET)
    file(GLOB_RECURSE  TARGET_HEADERS
          "${CMAKE_CURRENT_SOURCE_DIR}/*.h"
    )
    file(GLOB_RECURSE  TARGET_SRC
          "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp"
#          "${CMAKE_CURRENT_SOURCE_DIR}/*.qrc"
    )

    add_library(${_TARGET} STATIC ${TARGET_SRC} ${TARGET_HEADERS})      # +-may be we want SHARED
    add_library(synctables.libs::${_TARGET} ALIAS ${_TARGET})

    add_custom_command(
      TARGET ${_TARGET} PRE_BUILD
      COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_SOURCE_DIR}/lib
      VERBATIM
    )

    add_custom_command(
      TARGET ${_TARGET} POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E make_directory ${CMAKE_SOURCE_DIR}/lib_include
      COMMAND ${CMAKE_COMMAND} -E copy_if_different ${TARGET_HEADERS} ${CMAKE_SOURCE_DIR}/lib_include
      COMMENT "Copying ${_TARGET} public headers to ${CMAKE_SOURCE_DIR}/lib_include"
      VERBATIM
    )

#    INSTALL(
#        DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
#        DESTINATION ${CMAKE_SOURCE_DIR}/lib_include
#        FILES_MATCHING PATTERN "*.h*"
#    )
endfunction()

#добавляем цель исполняемого файла
function(custom_add_executable_from_dir _TARGET)
    file(GLOB_RECURSE TARGET_SRC
          "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp"
          "${CMAKE_CURRENT_SOURCE_DIR}/*.qrc"
    )
    add_executable(${_TARGET} ${TARGET_SRC})
endfunction()

